package be.kdg.java2.demostudents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemostudentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemostudentsApplication.class, args);
    }

}
