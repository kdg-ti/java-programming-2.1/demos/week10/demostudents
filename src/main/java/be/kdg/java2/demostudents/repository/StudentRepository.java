package be.kdg.java2.demostudents.repository;

import be.kdg.java2.demostudents.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    List<Student> findByBirthDayAfter(LocalDate aDate);
    List<Student> findByFirstName(String firstName);
}
