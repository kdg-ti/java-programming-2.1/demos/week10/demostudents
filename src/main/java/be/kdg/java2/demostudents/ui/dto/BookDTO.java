package be.kdg.java2.demostudents.ui.dto;

public class BookDTO {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
