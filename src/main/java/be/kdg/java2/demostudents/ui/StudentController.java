package be.kdg.java2.demostudents.ui;

import be.kdg.java2.demostudents.domain.Book;
import be.kdg.java2.demostudents.services.StudentService;
import be.kdg.java2.demostudents.ui.dto.BookDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StudentController {
    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping({"","/","/index"})
    public String getIndexPage(Model model){
        model.addAttribute("students", studentService.findAll());
        return "index";
    }

    @GetMapping("/student/{id}/detail")
    public String getStudentDetail(@PathVariable int id, Model model){
        System.out.println("Loading student by id...");
        model.addAttribute("student", studentService.findById(id));
        System.out.println("Loading the view...");
        return "student/detail";
    }

    @GetMapping("/student/{id}/delete")
    public String deleteStudent(@PathVariable int id, Model model){
        studentService.delete(id);
        return "redirect:/index";
    }

    @GetMapping("/student/youngsters")
    public String getYoungStudents(Model model){
        model.addAttribute("students", studentService.findYoungsters());
        return "student/youngsters";
    }

    @GetMapping("/student/jos")
    public String getJosList(Model model){
        model.addAttribute("students", studentService.findAllJos());
        return "student/jos";
    }

    @PostMapping("/student/{id}/addbook")
    public String addBookToStudent(@PathVariable int id, BookDTO bookDTO){
        studentService.addBook(id, new Book(bookDTO.getTitle()));
        return "redirect:/student/" + id + "/detail";
    }
}
