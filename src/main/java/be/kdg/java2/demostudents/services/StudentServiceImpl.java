package be.kdg.java2.demostudents.services;

import be.kdg.java2.demostudents.domain.Book;
import be.kdg.java2.demostudents.domain.Student;
import be.kdg.java2.demostudents.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService{
    private StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student findById(int id) {
        return studentRepository.findById(id).orElseThrow();
    }

    @Override
    public void delete(int id) {
        studentRepository.deleteById(id);
    }

    @Override
    public List<Student> findYoungsters() {
        LocalDate twentyYearsAgo = LocalDate.now().minusYears(20);
        return studentRepository.findByBirthDayAfter(twentyYearsAgo);
    }

    @Override
    public List<Student> findAllJos() {
        return studentRepository.findByFirstName("Jos");
    }

    @Override
    public void addBook(int studentId, Book book) {
        Student student = studentRepository.findById(studentId).orElseThrow();
        book.setStudent(student);
        student.getBooks().add(book);
        studentRepository.save(student);
    }
}
