package be.kdg.java2.demostudents.services;

import be.kdg.java2.demostudents.domain.Book;
import be.kdg.java2.demostudents.domain.Student;

import java.util.List;

public interface StudentService {
    List<Student> findAll();
    Student findById(int id);
    void delete(int id);
    List<Student> findYoungsters();
    List<Student> findAllJos();
    void addBook(int studentId, Book book);
}
